TEST PLAN OF THE STUDENT TELERIK FORUM  
https://schoolforum.telerikacademy.com/

1) Analysis of  the product
The forum will be used from the students of The school Telerik academy.The main reason for the existing of this forum is the students to share ideas or problems during the software courses, the problems to be resolved,  good ideas to be developed and productive discussions to help students to develop and upgrade their knowledge.
The forum must be simple and user- friendly communication tool. The tool must has log in and log out forms, simple menu, searching bar. In the main section each student must has possibility 
1.1. to create new topic with a name, description, possibility to attach file, edit the font, the size
1.2. to read all the topics and to comment them using “reply” button or to share in twitter, Facebook or by mail each topic
1.3. to bookmark each topic
Every user of the student forum must have possibility to access it from computer or mobile device independently from the operation system or the device browser.

2) Development of the Test Strategy
2.1. Definition of the Scope of Testing
 We will only focus at some functions and external interface of the webside https://schoolforum.telerikacademy.com/.  The function which will be tested are:   Creation of Topic, Creation of Comment, Set a user profile.   Functional testing, Usability testing (in scope testing)
Nonfunctional testing such as stress, performance, UI testing, security or logical database currently will not be tested. (out of scope).
2.2 Identification of the Testing Types
- Functional testing
- Usability testing
- Smoke testing
2.3 Creating Test Cases
-  Test cases will be executed by respective QA on client's development/test site based on designed scenarios, test cases and Test data.
- Test result (Actual Result, Pass/Fail) will updated in test case document Defect Logging and Reporting:
- QA will be logging the defect/bugs in Excel document, found during execution of test cases. After this, QA will inform respective the teacher about the defect/bugs.

 2.4. Creation of the Test Logistics
People responsible for the testing: Savina Ivancheva, Desislava Stoyanova, Aneliya Nestorova, Violeta Stoyanova. These people have strong ability to understand customers point of view, have attention to details and good cooperation.
We will start to test when we have all requirement documents and test environment.

3) Definition of the Test Objective
3.1. Functional tests
User account: 
-	New user / Prio1
-	Login      / Prio1
-	Log out  / Prio1
-	Change pass / Prio2
User possible functions;
-	See user messages / Prio2
-	See user preferences / Prio3
-	See user bookmarks  / Prio3
-	User search for topics, posts, users or categories  /Prio2
-	User to see the new topics / Prio2
-	User to create a new topic  / Prio1
-	User to edit the post         / Prio1
-	User to comment his own topic / Prio1
-	User to reply to another user topic / Prio1
-	User to like a comment  / Prio2
-	User to share the link to the post  / Prio3
-	User to bookmark the post  /Prio3

3.2. Usability tests
- Clear contents
- User-friendly design
3.3. Cross browser Testing
To verify if an application works across different browsers as expected and degrades gracefully.

4) Define Test Criteria
4.1. Entry Criteria
- All the necessary documentation, design, and requirements information should be available that will allow testers to operate the system and judge the correct  behavior.
- All   the standard software tools including the testing tools must have been successfully installed and functioning properly.
- Proper test data is available.
- The test environment such as, lab, hardware, software, and system administration support should be ready.
- QA resources have completely understood the requirements
- QA resources have sound knowledge of functionality

4.2. Exit Criteria
- 100% of High priority test cases pass
- 90% of Medium priority cases pass
- All high-risk areas have been fully tested, with only minor residual risks left outstanding.
- Cost – when the budget has been spent.
- The schedule has been achieved

5) System Resource Planning
Nr.	resources
1.     	Test tools	Sikuli and Selenium - to automate the testing, simulate the user operation, generate the test results.
2.     	Mobile device	The device which users will use to connect to web server of the forum
3.     	Computer	The PC which users often use to connect the web server

6) Test schedule
Task Name	                                          Start 	Finish	Effort	Comment
Test Planning				
Review Requirements documentation				
Functional Testing/black box testing				
Cross browser testing				
Usability Testing				

7) Test Deliverable
- Test results/report
-  Defect report
- Release note

