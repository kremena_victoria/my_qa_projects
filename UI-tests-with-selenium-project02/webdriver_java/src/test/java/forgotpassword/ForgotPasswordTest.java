package forgotpassword;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.EmailSentPage;
import pages.ForgotPasswordPage;

import static org.testng.Assert.*;

public class ForgotPasswordTest extends BaseTests {

    @Test
    public void testForgotPasswordField(){
        ForgotPasswordPage forgetPassPage = homepage.clickForgotPassword();
        forgetPassPage.setEmailAddress("kremenja@yahoo.com");

        EmailSentPage emailSentpage = forgetPassPage.clickSubmitButton();
        assertTrue(emailSentpage.getAlertText().contains("Your e-mail's been sent!"),"");
    }


}
