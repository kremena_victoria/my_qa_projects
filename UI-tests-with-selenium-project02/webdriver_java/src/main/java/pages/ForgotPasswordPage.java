package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ForgotPasswordPage {
    private WebDriver driver;
    private By forgotPassForm = By.id("email");
    private By retrievePassButton = By.id("form_submit");

    ForgotPasswordPage(WebDriver driver){
        this.driver = driver;
    }

    public void setEmailAddress(String emailAddress){
        driver.findElement(forgotPassForm).sendKeys(emailAddress);
    }

    public EmailSentPage clickSubmitButton(){
        driver.findElement(retrievePassButton).click();
        return new EmailSentPage(driver);
    }
}
